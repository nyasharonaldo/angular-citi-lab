import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Starship } from '../model/starship';
import { Person } from '../model/person';
import { Film } from '../model/film';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(private httpClient: HttpClient) { }

  public findStarships(): Observable<Starship[]> {
    return this.httpClient.get<Starship[]>(
              environment.backendUrl + 'starships/');
  }

  public findPeople(): Observable<Person[]> {
    return this.httpClient.get<Person[]>(
              environment.backendUrl + 'people/');
  }

  public findFilms(): Observable<Film[]>{
    return this.httpClient.get<Film[]>(
      environment.backendUrl + 'films/'
    );
  }

  public findStarshipByLink(link: string):Observable<Starship> {
    return this.httpClient.get<Starship>(
              link);
  }

  
}
