import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';
import { Person } from '../model/person';

@Component({
  selector: 'app-people-table',
  templateUrl: './people-table.component.html',
  styleUrls: ['./people-table.component.css']
})
export class PeopleTableComponent implements OnInit {
  people: Person[]
  constructor(private apiService: ApiServiceService) { }

  ngOnInit() {
    this.apiService.findPeople().subscribe(
      peopleArray => {
        console.log("Successfully retrieved starships")
        console.log(peopleArray.results);
        this.people = peopleArray.results;
      },
      error => {
        console.log(error);
      }
    );
  }

}
