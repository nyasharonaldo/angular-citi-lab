import { Component, OnInit } from '@angular/core';
import { Starship } from '../model/starship';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-startships-table',
  templateUrl: './startships-table.component.html',
  styleUrls: ['./startships-table.component.css']
})
export class StartshipsTableComponent implements OnInit {
  starships: Starship[];
  
  constructor(private apiService: ApiServiceService) { }

  ngOnInit() {
    this.apiService.findStarships().subscribe(
      starshipsArray => {
        console.log("Successfully retrieved starships")
        console.log(starshipsArray.results);
        this.starships = starshipsArray.results;
      },
      error => {
        console.log(error);
      }
    );
  }

}
