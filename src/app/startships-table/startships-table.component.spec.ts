import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartshipsTableComponent } from './startships-table.component';

describe('StartshipsTableComponent', () => {
  let component: StartshipsTableComponent;
  let fixture: ComponentFixture<StartshipsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartshipsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartshipsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
