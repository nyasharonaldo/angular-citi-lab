import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-citi-lab';
  selection: String
  starship_selected = true;
  people_selected: boolean;
  films_selected: boolean;
  constructor(){

  }

  getSelection(event){
    this.selection = event.target.value;
    console.log(event)
    if(this.selection == 'Starships'){
      this.starship_selected = true
      this.people_selected = false
      this.films_selected = false
    }else if(this.selection == 'People'){ 
      this.starship_selected = false
      this.people_selected = true
      this.films_selected = false
    }else if(this.selection == 'Films'){
      this.starship_selected = false
      this.people_selected = false
      this.films_selected = true
    }
    
  }


}
