export class Starship {
    constructor(
        public mglt: string,
        public cargo_capacity: number,
        public consumables: string,
        public cost_in_credits: number,
        public created: string,
        public crew: number,
        public edited: string,
        public hyperdrive_rating: number,
        public length: number,
        public manufacturer: string,
        public max_atmosphering_speed: string,
        public model: string,
        public name: string,
        public passengers: number,
        public films: string[],
        public pilots: string[],
        public starship_class: string,
        public url: string
    ){}
}
