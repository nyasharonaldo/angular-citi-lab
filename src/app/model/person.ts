import { ApiServiceService } from '../services/api-service.service';

export class Person {
    private api_service: ApiServiceService;
    constructor(
        public birth_year: string,
        public eye_color: string,
        public films: string[],
        public gender: string,
        public hair_color: string,
        public height: number,
        public homeworld: string,
        public mass: string,
        public name: string,
        public skin_colour: string,
        public created: string,
        public edited: string,
        public species: string[],
        public starships: string[],
        public url: string,
        public vehicles: string[]
    ){}
    
        
    
}
