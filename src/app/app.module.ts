import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { StartshipsTableComponent } from './startships-table/startships-table.component';
import { PeopleTableComponent } from './people-table/people-table.component';
import { FilmsTableComponent } from './films-table/films-table.component';

@NgModule({
  declarations: [
    AppComponent,
    StartshipsTableComponent,
    PeopleTableComponent,
    FilmsTableComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
