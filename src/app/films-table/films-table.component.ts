import { Component, OnInit } from '@angular/core';
import { Film } from '../model/film';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-films-table',
  templateUrl: './films-table.component.html',
  styleUrls: ['./films-table.component.css']
})
export class FilmsTableComponent implements OnInit {
  films: Film[];
  constructor(private apiService: ApiServiceService) { }

  ngOnInit() {
    this.apiService.findFilms().subscribe(
      filmsArray => {
        console.log("Successfully retrieved films")
        console.log(filmsArray.results);
        this.films = filmsArray.results;
      }
    )
  }

}
